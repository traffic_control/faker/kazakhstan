<?php

namespace Faker\Test\Kazakhstan;

use Faker\Generator;
use Faker\Kazakhstan\Address;
use PHPUnit\Framework\TestCase;

class AddressTest extends TestCase
{
    /**
     * @var Generator
     */
    private $_faker;

    public function setUp(): void
    {
        $faker = new Generator();
        $faker->seed(1);
        $faker->addProvider(new Address($faker));
        $this->_faker = $faker;
    }

    public function testCity()
    {
        $city = $this->_faker->city;
        $this->assertSame(true, is_string($city) && $city !== '', 'City name is not a valid string');
    }

    public function testStreet()
    {
        $street = $this->_faker->street;
        $this->assertSame(true, is_string($street) && $street !== '', 'Street name is not a valid string');
    }

    public function testRegion()
    {
        $region = $this->_faker->region;
        $this->assertSame(true, is_string($region) && $region !== '', 'Region name is not a valid string');
    }
}
