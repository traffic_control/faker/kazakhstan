<?php

namespace Faker\Kazakhstan;

use Faker\Extension\Extension;

class Color extends \Faker\Provider\Color implements Extension
{
    protected static $safeColorNames = [
        'қара', 'қою қызыл', 'жасыл', 'қара көк', 'сарғыш түс',
        'күлгін', 'көк', 'көк', 'күміс',
        'сұр', 'сары', 'қызылкүрең түс', 'теңіз толқыны түс', 'ақ'
    ];
}
