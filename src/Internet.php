<?php

namespace Faker\Kazakhstan;

use Faker\Extension\Extension;

class Internet extends \Faker\Provider\Internet implements Extension
{
    protected static $freeEmailDomain = ['mail.kz', 'yandex.kz', 'host.kz'];
    protected static $tld = ['com', 'com', 'net', 'org', 'kz', 'kz', 'kz', 'kz'];
}
