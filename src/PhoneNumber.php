<?php

namespace Faker\Kazakhstan;

use Faker\Extension\Extension;

class PhoneNumber extends \Faker\Provider\PhoneNumber implements Extension
{
    protected static $formats = [
        '+7 (701) #######',
        '+7 (702) #######',
        '+7 (705) #######',
        '+7 (707) #######',
        '+7 (727) 239####',
        '+7 (747) #######',
        '+7 (7172) 745###',
    ];
}
